
import { Routes } from '@angular/router';
import { AboutComponent } from './content/about/about.component';

const appRoutes: Routes = [
    {
        path: 'about',
        component: AboutComponent
    }
];
export default appRoutes;